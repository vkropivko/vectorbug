package app

import android.app.Application
import android.support.v7.app.AppCompatDelegate

class AppMain : Application() {


    init {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    override fun onCreate() {
        super.onCreate()

    }

}

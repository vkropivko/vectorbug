package app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import chapters.network.pojo.team.Player
import com.vectorbug.R
import kotlinx.android.synthetic.main.activity_test.*

class TestActivity : AppCompatActivity() {

    private var adapter:Adapter2?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        initRv()
    }

    private fun initPojo() {
        val player1 = Player().apply {
            progress = 2
            name="2"
        }
        val player2 = Player().apply {
            progress = 1
            name=" 1"
        }
        val player3 = Player().apply {
            progress = 4
            name="4"
            photo="http://www.ukrlitzno.com.ua/wp-content/uploads/2013/01/%D0%A2%D0%B0%D1%80%D0%B0%D1%81-%D0%A8%D0%B5%D0%B2%D1%87%D0%B5%D0%BD%D0%BA%D0%BE-%D0%B1%D1%96%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D1%96%D1%8F-%D1%81%D1%82%D0%B8%D1%81%D0%BB%D0%BE-1.jpg"
        }
        val player4 = Player().apply {
            progress = 3
            name="3"
        }
        val player5 = Player().apply {
            progress = 1
            photo = ""
            name=" 1"
        }
        val player6 = Player().apply {
            progress = 3
            name="3"
            photo="http://www.ukrlitzno.com.ua/wp-content/uploads/2013/01/%D0%A2%D0%B0%D1%80%D0%B0%D1%81-%D0%A8%D0%B5%D0%B2%D1%87%D0%B5%D0%BD%D0%BA%D0%BE-%D0%B1%D1%96%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D1%96%D1%8F-%D1%81%D1%82%D0%B8%D1%81%D0%BB%D0%BE-1.jpg"
        }
        val player7 = Player().apply {
            progress = 2
            name=" 2"
            photo="http://www.ukrlitzno.com.ua/wp-content/uploads/2013/01/%D0%A2%D0%B0%D1%80%D0%B0%D1%81-%D0%A8%D0%B5%D0%B2%D1%87%D0%B5%D0%BD%D0%BA%D0%BE-%D0%B1%D1%96%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D1%96%D1%8F-%D1%81%D1%82%D0%B8%D1%81%D0%BB%D0%BE-1.jpg"
        }
        val player8 = Player().apply {
            progress = 1
            name="1"
            photo="http://www.ukrlitzno.com.ua/wp-content/uploads/2013/01/%D0%A2%D0%B0%D1%80%D0%B0%D1%81-%D0%A8%D0%B5%D0%B2%D1%87%D0%B5%D0%BD%D0%BA%D0%BE-%D0%B1%D1%96%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D1%96%D1%8F-%D1%81%D1%82%D0%B8%D1%81%D0%BB%D0%BE-1.jpg"
        }
        val player9 = Player().apply {
            progress = 1
            name="1"
            photo="http://www.ukrlitzno.com.ua/wp-content/uploads/2013/01/%D0%A2%D0%B0%D1%80%D0%B0%D1%81-%D0%A8%D0%B5%D0%B2%D1%87%D0%B5%D0%BD%D0%BA%D0%BE-%D0%B1%D1%96%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D1%96%D1%8F-%D1%81%D1%82%D0%B8%D1%81%D0%BB%D0%BE-1.jpg"
        }
        val list= mutableListOf(player1, player2, player3, player4, player5,player6,player7,player8,player9)
        list.addAll(list)
        list.addAll(list)
        list.addAll(list)
        list.addAll(list)
        list.addAll(list)
        adapter?.list=list
    }

    private fun initRv() {
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rvTest.layoutManager = layoutManager
        adapter = Adapter2(this)
        rvTest.adapter = adapter
        initPojo()
    }
}
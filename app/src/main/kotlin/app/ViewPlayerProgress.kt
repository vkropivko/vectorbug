package chapters.ui.view


import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.RelativeLayout
import app.changeColorDrawable
import com.vectorbug.R
import kotlinx.android.synthetic.main.view_player_progress.view.*


class ViewPlayerProgress(context: Context?, attrs: AttributeSet?) : RelativeLayout(context, attrs) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_player_progress, this)
        //setProgress(progress)
    }

    fun setInfo(photo: String, progress: Int) {
        setProgress(progress)

    }

    private fun setProgress(progress: Int) {
        if (progress == 1) {
            viewlevel1.changeColorDrawable(R.color.colorAccent)
            viewLevel2.changeColorDrawable(R.color.divider)
            viewLevel3.changeColorDrawable(R.color.divider)
            viewlevel4.changeColorDrawable(R.color.divider)
        }
        else if (progress == 2) {
            viewlevel1.changeColorDrawable(R.color.colorAccent)
            viewLevel2.changeColorDrawable(R.color.green)
            viewLevel3.changeColorDrawable(R.color.divider)
            viewlevel4.changeColorDrawable(R.color.divider)
        }
        else if (progress == 3) {
            viewlevel1.changeColorDrawable(R.color.colorAccent)
            viewLevel2.changeColorDrawable(R.color.green)
            viewLevel3.changeColorDrawable(R.color.yellow)
            viewlevel4.changeColorDrawable(R.color.divider)
        }
        else if (progress == 4) {
            viewlevel1.changeColorDrawable(R.color.colorAccent)
            viewLevel2.changeColorDrawable(R.color.green)
            viewLevel3.changeColorDrawable(R.color.yellow)
            viewlevel4.changeColorDrawable(R.color.red)
        } else {
            viewlevel1.changeColorDrawable(R.color.divider)
            viewLevel2.changeColorDrawable(R.color.divider)
            viewLevel3.changeColorDrawable(R.color.divider)
            viewlevel4.changeColorDrawable(R.color.divider)
        }
    }
}
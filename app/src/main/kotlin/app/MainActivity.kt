package app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.vectorbug.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: Adapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRv()

    }

    private fun initRv() {
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rvTest.layoutManager = layoutManager
        adapter = Adapter(this)
        rvTest.adapter = adapter
        initPojo()
    }

    private fun initPojo() {
        val array = arrayListOf(0,1,2,3,4)
        array.addAll(array)
        array.addAll(array)
        array.addAll(array)
        array.addAll(array)
        Log.d("Size", array.size.toString())
        array.forEach {
            Log.d("LIST",it.toString())
        }
        adapter.list=array
        adapter.notifyDataSetChanged()
    }

}

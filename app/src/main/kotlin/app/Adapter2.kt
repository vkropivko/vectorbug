package app

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import chapters.network.pojo.team.Player
import chapters.ui.view.ViewPlayer
import com.vectorbug.R


class Adapter2(context: Context) : RecyclerView.Adapter<Adapter2.ViewHolderPlayer>() {

    private val inflater: LayoutInflater
    var list: List<Player>? = null

    init {

        inflater = LayoutInflater.from(context)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolderPlayer {
        return ViewHolderPlayer(inflater.inflate(R.layout.item_player_search, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolderPlayer?, position: Int) {
        holder?.bind(list?.get(position))
    }


    inner class ViewHolderPlayer(val view: View) : RecyclerView.ViewHolder(view) {

        private val viewPlayer: ViewPlayer

        init {
            viewPlayer = view.findViewById(R.id.viewPlayer) as ViewPlayer
        }

        fun bind(player: Player?) {
            viewPlayer.setInfo(player)
        }
    }
}
package app

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vectorbug.R
import java.util.*

class Adapter(val context: Context) : RecyclerView.Adapter<Adapter.TestViewHolder>() {


    var list: ArrayList<Int>? = null
    private val infalter: LayoutInflater

    init {

        infalter = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TestViewHolder {
        return TestViewHolder(infalter.inflate(R.layout.item_test, parent, false))
    }

    override fun onBindViewHolder(holder: TestViewHolder?, position: Int) {

        holder?.bind(list?.get(position) ?: 0)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }


    inner class TestViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val ivTest: AppCompatImageView
        private val tvColor: AppCompatTextView

        init {
            ivTest = view.findViewById(R.id.ivAndroid) as AppCompatImageView
            tvColor = view.findViewById(R.id.tvColor) as AppCompatTextView
        }

        fun bind(i: Int) {
            Log.d("adapter", i.toString())
            when (i) {
                0 -> {
                    DrawableCompat.setTint(ivTest.drawable, ContextCompat.getColor(context, R.color.red))
                    tvColor.text = "RED"
                }
                1 -> {
                    DrawableCompat.setTint(ivTest.drawable, ContextCompat.getColor(context, R.color.black))
                    tvColor.text = "BLACK"
                }
                2 -> {
                    DrawableCompat.setTint(ivTest.drawable, ContextCompat.getColor(context, R.color.blue))
                    tvColor.text = "BLUE"
                }
                3 -> {
                    DrawableCompat.setTint(ivTest.drawable, ContextCompat.getColor(context, R.color.green))
                    tvColor.text = "GREEN"
                }
                4 -> {
                    DrawableCompat.setTint(ivTest.drawable, ContextCompat.getColor(context, R.color.yello))
                    tvColor.text = "YELLO"
                }
            }

        }
    }
}

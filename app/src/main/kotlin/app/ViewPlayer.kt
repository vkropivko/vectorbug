package chapters.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import chapters.network.pojo.team.Player
import com.vectorbug.R
import kotlinx.android.synthetic.main.view_player.view.*


class ViewPlayer(context: Context?, attrs: AttributeSet?) : RelativeLayout(context, attrs) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_player, this)
    }

    fun setInfo(player: Player?) {
        viewPlayerProgress.setInfo(player?.photo ?: "", player?.progress ?: 1)
        tvName.text=player?.name
//        tvCity.text=player?.city
//        tvPosition.text=player?.position
    }
}
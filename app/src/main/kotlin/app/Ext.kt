package app

import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.widget.AppCompatImageView


fun AppCompatImageView.changeColorDrawable(color: Int) {
    DrawableCompat.setTint(this.drawable, ContextCompat.getColor(this.context, color))
}